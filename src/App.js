import React, { useState, useEffect } from "react";
import './App.css';
import Layout from "./components/Layout/Layout";
import Routes from "./Routes";
import { BrowserRouter } from "react-router-dom";

export const StateContext = React.createContext("my_context");

function App() {
    const [data, setData] = useState([]);

    useEffect(() => {
        fetch("https://raw.githubusercontent.com/fortemka/test_task/master/test-task-feed_en.json")
            .then(response => response.json())
            .then(data => setData(data));
    });

    console.log("data", JSON.stringify(data));

    return (
        <BrowserRouter>
            <div className="App">
                <StateContext.Provider value={data}>
                    <Layout>
                        <Routes />
                    </Layout>
                </StateContext.Provider>
            </div>
        </BrowserRouter>
    );
}

export default App;
