import React from "react";

import styles from "./search.module.css";

export default function Search() {
    return (
        <form className={styles.search}>
            <input type="text" className={styles.input} />
            <button className={styles.btn}>Find</button>
        </form>
    )
}

// class Search extends React.Component {
//     render() {
//         return (
//             <form>
//                 <input type="text"/>
//                 <button>Find</button>
//             </form>

//         )
//     }
// }

// export default Search;