import React, { useContext } from "react";
import { useParams } from 'react-router-dom';
import { StateContext } from "../../App";

export default function Category(props) {
    const data = useContext(StateContext);
    console.log("category context", data);

    console.log("category props", props);
    // console.log("props.data", props.data);
    // console.log("props.match.path", props.match.path);
    // console.log("props.match.url", props.match.url);

    let { name } = useParams();
    console.log("name", name);

    let filterGames = (games, categoryName) => {
        if (categoryName) {
            console.log("categoryName", categoryName)
            var filteredGames = [];
            games.forEach(game => {
                game.feedMetadata.categories.forEach(category => {
                    if (category === categoryName) {
                        // console.log("game", game);
                        filteredGames.push(game);
                    }
                })
            });
            // return filteredGames;
            return filteredGames.map((game, i) => {
                return (
                    <div key={i}>
                        <div>{game.name}</div>
                        <div>{game.description}</div>
                    </div>
                )
            })
        }
    }

    return (
        <div>
            <div>Category</div>
            <div>{filterGames(data, name)}</div>

        </div>
    )
}