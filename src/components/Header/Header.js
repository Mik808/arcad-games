import React from "react";
import Search from "../Search/Search";

import styles from "./header.module.css";

class Header extends React.Component {
    render() {
        return (
            <div className={styles.header}>
                <div className={`title ${styles.title}`}>ArenaName</div>
                <Search />
            </div>

        )
    }
}

export default Header;