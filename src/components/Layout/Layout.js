import React from "react";
import Header from "../Header/Header";
import Footer from "../Footer/Footer"
import { useContext } from "react";
import { StateContext } from "../../App";

export default function Layout(props) {
    const data = useContext(StateContext);
    console.log("context layout", data);

    return (
        <div>
            <Header />
            {props.children}
            <Footer data={data}/>
        </div>
    )
}
